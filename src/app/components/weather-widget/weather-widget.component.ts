import { Component, ElementRef, OnInit, ViewChild, Renderer2, AfterViewInit } from '@angular/core';
import { ApiService, GeoData } from '@services/api.service';
import { MatSelect } from '@angular/material';
import { fromEvent } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';


@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.less']
})
export class WeatherWidgetComponent implements OnInit, AfterViewInit {

  @ViewChild('cityIdRef', { static: false }) cityIdRef: MatSelect;
  @ViewChild('cityNameInputRef', { static: false }) cityNameInputRef: ElementRef;

  private geoLocation: GeoData = {
    latitude: null,
    longitude: null
  };
  public showData = false;
  public weatherData: any = null;
  public location: string;
  public cityName: any;
  public cityId: any;
  public errorMsg: any;
  public showError = false;
  public cities: any[];
  constructor(
    private apiService: ApiService,
    private elementRef: ElementRef,
    private renderer: Renderer2,
  ) { }

  ngOnInit() {
    navigator.geolocation.getCurrentPosition((geodata) => {
      this.geoLocation.latitude = geodata.coords.latitude;
      this.geoLocation.longitude = geodata.coords.longitude;
      this.getByLocation();
    });
  }

  ngAfterViewInit() {
    this.handlerInputCityName(this.cityNameInputRef.nativeElement);
  }

  /**
   * обращаемся к сервису для получения данных по имени города
   *
   * @author A.Bondarenko
   * @date 2020-02-13
   * @memberof WeatherWidgetComponent
   */
  public getWeather() {
    if (this.cityName) {
      this.cities = [];
      this.apiService.find(this.cityName).subscribe(
        (data) => {
          if (data && data.length > 0) {
            this.cities = data;
            this.cityId = data[0].id;
            this.createDataForForm(data[0]);
          } else {
            this.showData = false;
            this.showError = true;
            this.errorMsg = 'Check name of city';
          }
        },
        (error) => {
          this.showData = false;
          this.showError = true;
          this.errorMsg = error.error.message;
        }
      );
    }
  }

  /**
   * запрашиваем погоду по геолокации (данные берем из браузера)
   *
   * @author A.Bondarenko
   * @date 2020-02-13
   * @memberof WeatherWidgetComponent
   */
  public getByLocation() {
    this.cities = [];
    this.apiService.getByGeoCoordinates(this.geoLocation).subscribe(
      (data) => {
        this.cityName = null;
        this.createDataForForm(data);
      }
    );
  }

  /**
   * отображаем данные на форме
   *
   * @author A.Bondarenko
   * @date 2020-02-13
   * @private
   * @param {*} data
   * @memberof WeatherWidgetComponent
   */
  private createDataForForm(data) {
    this.showError = false;
    this.weatherData = data;
    this.showData = true;
  }

  /**
   * отслеживаем выбор города из списка
   * если у нас по запрашиваему имени города пришло несколько городов
   *
   * @author A.Bondarenko
   * @date 2020-02-13
   * @param {*} data
   * @memberof WeatherWidgetComponent
   */
  public dropdownChanged(data) {
    const id = this.cities.find((city) => {
      return city.id === data;
    });
    this.createDataForForm(id);
  }

  /**
   * подписываемся и прослушиваем ввод в поле input
   * ставим задержку 700мс
   * и запрашиваем город только если введено больше 2 символов
   *
   * @author A.Bondarenko
   * @date 2020-02-13
   * @private
   * @param {HTMLElement} node
   * @memberof WeatherWidgetComponent
   */
  private handlerInputCityName(node: HTMLElement) {
    fromEvent(node, 'keyup').pipe(
      debounceTime(700),
      filter((data: any) => {
        return data.target.value.length >= 2;
      })
    ).subscribe(
      (data) => {
        this.getWeather();
      }
    );
  }
}
